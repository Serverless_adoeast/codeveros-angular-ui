import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../user.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';

@Component({
  templateUrl: './user-dialog.component.html',
  styleUrls: [ './user-dialog.component.scss' ]
})
export class UserDialogComponent implements OnInit {
  user: User;
  dialogForm: FormGroup;
  isSaving = false;
  isEdit = false;
  title: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UserDialogComponent>,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.user = data.user || {};
  }

  get email() {
    return this.dialogForm.get('email');
  }

  ngOnInit(): void {
    this.isEdit = !!(this.user && this.user._id);
    this.title = this.isEdit ? 'Edit User' : 'Add User';

    this.dialogForm = this.formBuilder.group({
      username: [ this.user.username, Validators.required ],
      firstName: [ this.user.firstName, Validators.required ],
      lastName: [ this.user.lastName, Validators.required ],
      email: [ this.user.email, [ Validators.required, Validators.email ] ],
    });
  }

  onSubmit() {
    if (this.isSaving || this.dialogForm.invalid) {
      return;
    }

    this.isSaving = true;

    const value: User = this.dialogForm.value;

    const request = (this.isEdit) ?
      this.userService.updateUser(this.user._id, value) :
      this.userService.createUser(value);

    request.subscribe( (returnValue: User) => {
      this.isSaving = false;
      this.dialogRef.close(returnValue);
    });
  }
}
